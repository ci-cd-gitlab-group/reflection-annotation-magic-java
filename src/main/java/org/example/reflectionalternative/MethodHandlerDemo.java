package org.example.reflectionalternative;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.VarHandle;

public class MethodHandlerDemo {

    public static void main(String[] args) throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        Student student = new Student("Aniket" , "Java");
        //Find the class
        Class<?> aClass = lookup.findClass(Student.class.getName());

       //Method  Handling .....
        MethodType methodType = MethodType.methodType(String.class); //Giving return type as Input parameter is None
        MethodHandle getCourse = lookup.findVirtual(aClass, "getCourse", methodType);
        Object invoke = getCourse.invoke(student);
        System.out.println(invoke);

        //Method handling with Inpout params
         methodType = MethodType.methodType(void.class , String.class); //Giving return type and  Input parameter
        MethodHandle setCourse = lookup.findVirtual(aClass, "setCourse", methodType);
        setCourse.invoke(student , "Music");
        System.out.println(student.getCourse());

        //Invoking Static Method
        methodType = MethodType.methodType(void.class , int.class); //Giving return type and  Input parameter
        MethodHandle setNumOfStudent = lookup.findStatic(aClass, "setNumOfStudent", methodType);
        setNumOfStudent.invoke(  10); //Here we do not need Object as its static Method
        System.out.println(Student.getNumOfStudent());

        //Constructor Handling
        MethodType constructorType = MethodType.methodType(void.class);//Default cponstructor
        MethodHandle constructor = lookup.findConstructor(aClass, constructorType);
        Student StdObj = (Student) constructor.invoke(); // Calling default Constructor
        StdObj.setName("Amit");
        StdObj.setCourse("Angualr");
        System.out.println(StdObj);

        //Constructor with Parameters

         constructorType = MethodType.methodType(void.class , String.class , String.class);//Default cponstructor
         constructor = lookup.findConstructor(aClass, constructorType);
         StdObj = (Student) constructor.invoke("Ashoke" , "React"); // Calling default Constructor
        System.out.println(StdObj);

        //Usage of Find Getter and findSetter
        //They directly get value from field - if field is not private - else will get exception
        //They do not call the actual Getter method
        MethodHandles.Lookup privateLookupIn = MethodHandles.privateLookupIn(aClass, lookup);
        //MethodHandle nameGetter = lookup.findGetter(aClass, "name", String.class); //Last param is Return type
        //MethodHandle nameSetter = lookup.findSetter(aClass, "name", String.class); //Last param is Param  type

        //nameGetter.invoke(student);// - Will give exception
        //nameSetter.invoke(student , "King");// - Will give exception

         //Because Now we are using privateLookupIn - we can access private member directly
        MethodHandle nameGetter = privateLookupIn.findGetter(aClass, "name", String.class); //Last param is Return type
        MethodHandle nameSetter = privateLookupIn.findSetter(aClass, "name", String.class); //Last param is Param  type


        nameSetter.invoke(student , "King");
        System.out.println(nameGetter.invoke(student));

        //Checking with Var handle -----------
         //privateLookupIn is used because the variable is private in nature
        VarHandle course = privateLookupIn.findVarHandle(aClass, "course", String.class);
        System.out.println(course.get(student));
        course.set(student  , "Kotlin");
        System.out.println(course.get(student));
    }
}
