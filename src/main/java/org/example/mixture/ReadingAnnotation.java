package org.example.mixture;

import org.example.annotation.MostUsed;
import org.example.annotation.Utility;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReadingAnnotation {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<?> utility = Class.forName("org.example.annotation.Utility");
        Constructor<?> constructor = utility.getConstructor();
        Utility utilityInstance = (Utility) constructor.newInstance();

        Method[] methods = utility.getDeclaredMethods();
        for(Method method : methods){
            method.setAccessible(true);
            MostUsed annotation = method.getAnnotation(MostUsed.class);
            String value = annotation.value();
            if(method.isAnnotationPresent(MostUsed.class)){
                if(value.equals("Java")){
                    method.invoke(utilityInstance , "Aniket Roy");
                }else{
                    method.invoke(utilityInstance , 100);
                }

            }
        }
    }
}
