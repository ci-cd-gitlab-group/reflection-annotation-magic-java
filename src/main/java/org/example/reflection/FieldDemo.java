package org.example.reflection;

import java.lang.reflect.Field;
import java.util.Arrays;

public class FieldDemo {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        FieldDemoPojo fieldDemo = new FieldDemoPojo("Aniket", 10);
        Class<? extends FieldDemoPojo> aClass = fieldDemo.getClass();
        Field[] fields = aClass.getFields(); //This will only give Public Fields
        System.out.println("Public  Fields --- " + Arrays.asList(fields));

        fields = aClass.getDeclaredFields(); //This will only give Public Fields
        System.out.println("All  Fields --- " + Arrays.asList(fields));

        Field isMarried = aClass.getField("isMarried"); // Because its a public field
        System.out.println(isMarried.getName());
        Field parentName = aClass.getField("parentName"); // Because its a public field of parent class
        System.out.println(parentName.getName());
        System.out.println(fieldDemo.toString());
        Field age = aClass.getDeclaredField("age");
        age.setAccessible(true);
        age.set(fieldDemo , 40);
        System.out.println(fieldDemo.toString());

    }
}
