package org.example.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class MethodDemo {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

        FieldDemoPojo fieldDemo = new FieldDemoPojo("Aniket", 10);
        Class<? extends FieldDemoPojo> aClass = fieldDemo.getClass();
        Method[] methods = aClass.getMethods();//This will only give Public Methods  of this and parent class
        System.out.println("Public  Methods --- " + Arrays.asList(methods));

        methods = aClass.getDeclaredMethods(); //This will  give all Methods of this class
        System.out.println("All  Methods --- " + Arrays.asList(methods));

        Method fun = aClass.getMethod("fun");// Because its a public method
        fun.invoke(fieldDemo);
        Method call = aClass.getMethod("call");// Because its a public method
        call.invoke(fieldDemo);
        Method getName = aClass.getDeclaredMethod("getName", int.class);//Private , protected , default methof of this class  method
        getName.setAccessible(true);
        System.out.println(getName.invoke(fieldDemo , 22));

    }
}
