package org.example.reflection;

import java.util.Arrays;

public class ClassDemo {

    public static void main(String[] args) {
        //Ways to create Class<myClass> Object
        try {
            Class<?> aClass1 = Class.forName("java.lang.String");
            Class<?> aClass2 = Class.forName("java.lang.String");
            if(aClass1 == aClass2){
                System.out.println(" They are actually pointing to same Instance ");
            }
            //If we have known the ClassName - we can use that to get instance of Class instance - with Name.class extenstion
            //This will work for Void , Premitivies , and Anyother classes
            Class<Student> studentClass = Student.class;
            Class<Integer> integerClass = int.class;
            Class<Void> voidClass = void.class;

            //Using getClass obn Object  - We know Object is parent of all Class - It has getClass method
            DummyClass dummyClass = new DummyClass();
            Class<? extends DummyClass> aClass = dummyClass.getClass();
            //Getting Superclass of current class

            Class<?> superclass = aClass.getSuperclass();
            System.out.println(" Superclass = "+superclass);

            //Getting implemented Interfaces
            Class<?>[] interfaces = aClass.getInterfaces();
            System.out.println(" Implemented Interfaces = "+ Arrays.asList(interfaces));
            System.out.println(aClass.getName());

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
