package org.example.reflection;

public class FieldDemoPojo extends  FieldDemoParentPojo {

    String name;
    int age;

    public  boolean isMarried = false;

    public FieldDemoPojo(String name, int age) {
        super();
        this.name = name;
        this.age = age;
    }

    public FieldDemoPojo() {
        super();
    }

    private FieldDemoPojo(String name){
        super();
        this.name = name;
    }

    @Override
    public String toString() {
        return "FieldDemoPojo{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //@Override
    public void call(int d){
        System.out.println(" I am call in child");
    }

    public void fun(){
        System.out.println(" I am fun in child");
    }

    private String getName(int a){
        System.out.println(" My age is "+ a);
        return "Aniket is "+a;
    }
}
