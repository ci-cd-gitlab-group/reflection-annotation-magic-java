package org.example.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class ConstructorDemo {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {

        FieldDemoPojo fieldDemo = new FieldDemoPojo("Aniket", 10);
        Class<? extends FieldDemoPojo> aClass = fieldDemo.getClass();
        // TODO - WILL ONLY GIVE OUBLUC CONSTRUCTORS OF THIS CLASS - NOT OF PARENT CLASS
        Constructor<?>[] constructors = aClass.getConstructors();//This will only give Public Constructors only of this class
        System.out.println("Public  constructors --- " + Arrays.asList(constructors));

        constructors = aClass.getDeclaredConstructors(); //This will get all constructors of this class
        System.out.println("All  constructors --- " + Arrays.asList(constructors));

        Constructor<? extends FieldDemoPojo> constructor = aClass.getConstructor(String.class, int.class);// Because its a public Constructors
        System.out.println(constructor.newInstance("Aniket" , 23));
        Constructor<? extends FieldDemoPojo> declaredConstructor = aClass.getDeclaredConstructor(String.class);// Because its a private  Constructors of this  class
        declaredConstructor.setAccessible(true);
        System.out.println(declaredConstructor.newInstance("Aniket"));

    }
}
