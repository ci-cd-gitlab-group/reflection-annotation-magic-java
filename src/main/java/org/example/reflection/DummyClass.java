package org.example.reflection;

import java.io.*;

public class DummyClass extends Thread implements Serializable , Externalizable {
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }
}
