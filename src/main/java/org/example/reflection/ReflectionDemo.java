package org.example.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ReflectionDemo {

    public static void main(String[] args) {
       // Student student = new Student(); //Not Possible for Private Constructor

        //Using reflection
        try {
            Class<?> aClass = Class.forName("org.example.reflection.Student");
            Constructor<?> declaredConstructor = aClass.getDeclaredConstructor();
            declaredConstructor.setAccessible(Boolean.TRUE); //Now constructor Public
            Student student = (Student) declaredConstructor.newInstance();
            System.out.println(student.name);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
