package org.example.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ModifierDemo {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException {

        FieldDemoPojo fieldDemo = new FieldDemoPojo("Aniket", 10);
        Class<? extends FieldDemoPojo> aClass = fieldDemo.getClass();
        int modifiers = aClass.getModifiers();

        System.out.println("Class Modifier  --- " + modifiers + "-----" + Modifier.toString(modifiers));


        Field isMarried = aClass.getField("isMarried");
        modifiers = isMarried.getModifiers();
        System.out.println("Filed Modifier  --- " + modifiers + "-----" + Modifier.toString(modifiers));

        Method getName = aClass.getDeclaredMethod("getName", int.class);//Private , protected , default methof of this class  method
        modifiers = getName.getModifiers();
        System.out.println("Method Modifier  --- " + modifiers + "-----" + Modifier.toString(modifiers));


    }
}
