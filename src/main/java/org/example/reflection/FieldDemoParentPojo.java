package org.example.reflection;

public class FieldDemoParentPojo {
    public FieldDemoParentPojo(String parentName, int parentage, String protectedField) {
        this.parentName = parentName;
        this.parentage = parentage;
        this.protectedField = protectedField;
    }


    public FieldDemoParentPojo() {
    }

    public String parentName;
     int parentage;

     protected  String protectedField;


     public void call(){
         System.out.println(" I am call in parent");
     }
}
