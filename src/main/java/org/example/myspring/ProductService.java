package org.example.myspring;

import java.util.List;

@Component // Only for these Instance will get created
public class ProductService {

    //Now this will create Object everytime This class Instance get created - which we dont want
    //private ProductRepository productRepository = new ProductRepository();

    @Autowired
    private ProductRepository productRepository ;

    List<Product> getUpdatedPricedProduct(){
        List<Product> inital = productRepository.getProductList();
        inital.forEach(item->{
            item.setPrice(item.getPrice()-item.getDiscount());
        });
        return inital;
    }

    public static void main(String[] args) {
        System.out.println(new ProductService().getUpdatedPricedProduct());
    }

}
