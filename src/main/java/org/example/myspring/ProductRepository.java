package org.example.myspring;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class ProductRepository {

    @Autowired
    private  ProductDatabase productDatabase;
    public List<Product> getProductList(){
        List<Product> products = productDatabase.getProductList();
        products.stream().forEach(item->item.setPrice(Double.valueOf(1000)));
        return products;
    }

    public static void main(String[] args) {
        System.out.println(new ProductRepository().getProductList());
    }
}
