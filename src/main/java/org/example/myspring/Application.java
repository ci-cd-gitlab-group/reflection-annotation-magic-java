package org.example.myspring;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class Application {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        ApplicationContext applicationContext = new ApplicationContext(AppConfig.class);

        //ProductService productService = new ProductService();
        ProductService productService = applicationContext.getBean(ProductService.class);
        List<Product> updatedPricedProduct = productService.getUpdatedPricedProduct();
        System.out.println(updatedPricedProduct);
    }
}
