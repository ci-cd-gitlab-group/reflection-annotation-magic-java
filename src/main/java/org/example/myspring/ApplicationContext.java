package org.example.myspring;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.FileSystems;
import java.util.HashMap;
import java.util.Map;

//Here the App resources are made available - this is the IAC Container
public class ApplicationContext {

    //This will store all Instance classes
    private final static Map<Class<?> , Object> instanceMap = new HashMap<>();


    public ApplicationContext(Class<AppConfig> appConfigClass) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Spring.init(appConfigClass);
    }


    //Here because class is not of Generic Type - So in method Declaration we need to
    //Give about Type <T> is present before return type for this reason
    public <T> T getBean(Class<T> aClass) throws IllegalAccessException {
         T object = (T) instanceMap.get(aClass);
        Field[] declaredFields = aClass.getDeclaredFields();
        injectBean(object , declaredFields);
        return object;
    }

    private <T> void injectBean(T object, Field[] declaredFields) throws IllegalAccessException {
        for(Field field : declaredFields){
            if(field.isAnnotationPresent(Autowired.class)){
                field.setAccessible(true);
                Class<?> type = field.getType();
                Object inject = instanceMap.get(type);
                field.set(object,inject);
                //Now recursively Do for all Other Ones
                Field[] declaredFieldsInner = type.getDeclaredFields();
                injectBean(inject,declaredFieldsInner);
            }
        }
    }

    private static class Spring {

        public static void init(Class<AppConfig> appConfigClass) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
            //Check if the Class is a Configuration class or Not

            if(appConfigClass.isAnnotationPresent(Configuration.class)){
                ComponentScan annotation = appConfigClass.getAnnotation(ComponentScan.class);
                System.out.println("Scanning package = "+ annotation.value());
                String packageStructure = "target/classes/"+ annotation.value().replaceAll("\\.", "/");
                System.out.println(" File path = " +packageStructure);
                System.out.println(new File("").getAbsolutePath());
                File[] files = findClassesInPath(new File(packageStructure));
                for (File file:files) {
                    String name  = annotation.value() + "." + file.getName().replace(".class" , "");
                    System.out.println("Loading class ===" +name);
                    Class<?> aClass = Class.forName(name);
                    if(aClass.isAnnotationPresent(Component.class)){
                        Constructor<?> constructor = aClass.getConstructor();
                        Object instance = constructor.newInstance();
                        instanceMap.put(aClass,instance);
                    }
                }
                System.out.println(instanceMap);

            }else{
                throw new RuntimeException("Not an Configuration class");
            }
        }

        private static File[] findClassesInPath(File file) {

            if(!file.exists()){
                throw new RuntimeException("Path is Not valid !!");
            }else {
                File[] files = file.listFiles(e -> e.getName().endsWith(".class"));
                return files;
            }
        }
    }
}
