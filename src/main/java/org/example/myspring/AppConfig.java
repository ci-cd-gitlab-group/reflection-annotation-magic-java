package org.example.myspring;
//This is the Configuration file - we need to annotate it

@Configuration
@ComponentScan("org.example.myspring")
public class AppConfig {
    //we can create beans when Spring application start
}
