package org.example.myspring;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class ProductDatabase {


    public List<Product> getProductList(){
        List<Product> products = IntStream.range(0, 20).mapToObj(itme -> {
            return new Product("Product-" + itme, new Random().nextInt(50));
        }).collect(Collectors.toList());
        return products;
    }

    public static void main(String[] args) {
        System.out.println(new ProductDatabase().getProductList());
    }
}
