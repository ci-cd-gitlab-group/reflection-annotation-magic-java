package org.example.annotation;

import java.util.ArrayList;

public class Child  extends  Parent{


    public static void main(String[] args) {
        myInf myInf = ()->System.out.println(" Iam all Good ");
        myInf.fundo();
    }
    @SuppressWarnings("unused")
    int a = 100;

    @SuppressWarnings( {"rawtypes" , "unused"}) // No need to write value = [..]
    ArrayList uncheckedArr = new ArrayList();

    @Override  //this will give error if method signature change
   public void call(){
       System.out.println(" I am parent class ");
   }

   @Deprecated(since = "2" , forRemoval = false)
   public void old(){

   }
}

@FunctionalInterface //We can now only add one Abstract method on this Interface else it will give compilation error
interface  myInf {
    public void fundo();
}
