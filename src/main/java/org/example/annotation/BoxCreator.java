package org.example.annotation;

public class BoxCreator {

    public static void main(String[] args) {
        Box<String> stringBox  = new @NotEmpty Box<>("Aniket" ,"Roy" );
        stringBox.new NestedBox<String>(  "Amit" , "Roy");

    }

}
