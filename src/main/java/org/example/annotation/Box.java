package org.example.annotation;

public class Box< @NotEmpty T> {

    @NotEmpty
    String name;
    T genericType;

    public Box(String name,  @NotEmpty T genericType) {
        this.name = name;
        this.genericType = genericType;
    }

    class NestedBox<S> extends  Box<T>{

        public NestedBox(String name, @NotEmpty T genericType) {
            super(name, genericType);
        }
    }
}
