package org.example.annotation;

import java.lang.annotation.*;

@Documented //Now this will be part of Documentation of where its used
@Inherited // Now if we put this Annottion in a class level - Its childs will automatically inherit the Annotation
@Target({ElementType.METHOD , ElementType.TYPE}) //Is it for var , constructor , method ,  etc ...
@Retention(RetentionPolicy.RUNTIME)// SOURCE ,CLASS , RUNTIME  - used to define lifespan of the Annotatiion
//SOURCE === upto code compilation before that point  - THEN DISCARDED
//CLASS = uto dot class file generation
//RUNTIME = till the runtime
public @interface MostUsed {
    String value() default "Java";
}
