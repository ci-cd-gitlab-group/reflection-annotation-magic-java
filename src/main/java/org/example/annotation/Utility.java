package org.example.annotation;
@MostUsed
public class Utility {

    void doStuff(){
        System.out.println("Perform some stuff");
    }

    @MostUsed("Scala")
    void doStuff(int a){
        System.out.println("Perform some stuff --- " + a);
    }
    @MostUsed() //Will take Java
    void doStuff(String data){
        System.out.println("Perform some stuff ---" + data);
    }
}
