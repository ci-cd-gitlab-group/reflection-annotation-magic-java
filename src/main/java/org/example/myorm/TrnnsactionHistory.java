package org.example.myorm;

import java.math.BigDecimal;

public class TrnnsactionHistory {

    @PrimaryKey
    long transactionId; //User will not be providing it - will be auto Generate
   @Column
    int accountnumber;
    @Column
    String name;
    @Column
    String transactionType;
    @Column
    BigDecimal amount;

    public TrnnsactionHistory() {
    }

    public TrnnsactionHistory(int accountnumber, String name, String transactionType, BigDecimal amount) {

        this.accountnumber = accountnumber;
        this.name = name;
        this.transactionType = transactionType;
        this.amount = amount;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public int getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(int accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TrnnsactionHistory{" +
                "transactionId=" + transactionId +
                ", accountnumber=" + accountnumber +
                ", name='" + name + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", amount=" + amount +
                '}';
    }
}
