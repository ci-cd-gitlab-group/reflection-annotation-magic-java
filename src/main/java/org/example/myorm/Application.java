package org.example.myorm;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.SQLException;

public class Application {

    public static void main(String[] args) throws SQLException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException {



        TrnnsactionHistory trnnsactionHistory1 = new TrnnsactionHistory();
        trnnsactionHistory1.setAccountnumber(100100100);
        trnnsactionHistory1.setName("Aniket Roy");
        trnnsactionHistory1.setAmount(BigDecimal.valueOf(100.23));
        trnnsactionHistory1.setTransactionType("Debit");

        TrnnsactionHistory trnnsactionHistory2 = new TrnnsactionHistory();
        trnnsactionHistory2.setAccountnumber(200200200);
        trnnsactionHistory2.setName("Amit Roy");
        trnnsactionHistory2.setAmount(BigDecimal.valueOf(1004.23));
        trnnsactionHistory2.setTransactionType("Credit");

        TrnnsactionHistory trnnsactionHistory3 = new TrnnsactionHistory();
        trnnsactionHistory3.setAccountnumber(300300300);
        trnnsactionHistory3.setName("Aloke Roy");
        trnnsactionHistory3.setAmount(BigDecimal.valueOf(140.23));
        trnnsactionHistory3.setTransactionType("Credit");

        TrnnsactionHistory trnnsactionHistory4 = new TrnnsactionHistory();
        trnnsactionHistory4.setAccountnumber(400400400);
        trnnsactionHistory4.setName("Ashoke Roy");
        trnnsactionHistory4.setAmount(BigDecimal.valueOf(300.23));
        trnnsactionHistory4.setTransactionType("Debit");

        Hibernate<TrnnsactionHistory> hibernate = Hibernate.getConnection();
        hibernate.write(trnnsactionHistory1);
        hibernate.write(trnnsactionHistory2);
        hibernate.write(trnnsactionHistory3);
        hibernate.write(trnnsactionHistory4);

        TrnnsactionHistory trnnsactionHistory = hibernate.read(TrnnsactionHistory.class , 2L);

        System.out.println(" Reads from SQL DB === "+ trnnsactionHistory.toString());

    }
}
