package org.example.myorm;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Hibernate<T> {
    private Connection connection;
    private AtomicLong atomicLongId;
    public static  <T>   Hibernate<T> getConnection() throws SQLException, ClassNotFoundException {
        return new Hibernate<T>();
    }

    private Hibernate() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connection = DriverManager.getConnection(  "jdbc:mysql://localhost:3306/db?allowPublicKeyRetrieval=true&useSSL=false",
                "root", "root");
        this.atomicLongId = new AtomicLong(0L);
    }

    public void write(T t) throws IllegalAccessException, SQLException {
        Class<?> aClass = t.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();
        Field parimaryKey = null;
        List<Field> columns = new ArrayList<>();
        StringJoiner stringJoiner = new StringJoiner(",");
        for(Field field : declaredFields){
            if(field.isAnnotationPresent(PrimaryKey.class)){
                System.out.println(" Primary Key == "+ field.getName() + "| value = " + field.get(t));
                parimaryKey = field;
                stringJoiner.add(field.getName());
            }else   if(field.isAnnotationPresent(Column.class)){
                System.out.println("Column = "+field.getName()+ "| value = " + field.get(t));
                columns.add(field);
                stringJoiner.add(field.getName());
            }
        }
        String placeHolders = IntStream.range(0, declaredFields.length).mapToObj(itm -> "?").collect(Collectors.joining(","));
        String sqlQuery = "insert into "+ aClass.getSimpleName() + "("+stringJoiner+")" + " values (" +placeHolders+ ")";
        System.out.println("QUERY === "+ sqlQuery);
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setObject(1,atomicLongId.incrementAndGet());
        int paramIndex = 2;
        for(Field field : columns){
          //In case Field is private
            field.setAccessible(true);
            preparedStatement.setObject(paramIndex++ ,field.get(t) );
            field.setAccessible(false);
        }
        preparedStatement.execute();
    }

    public T read(Class<T> aClass, long l) throws SQLException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Field[] declaredFields = aClass.getDeclaredFields();
        Field parimaryKey = null;
        for(Field field : declaredFields){
            if(field.isAnnotationPresent(PrimaryKey.class)){
                System.out.println(" Primary Key == "+ field.getName() );
                parimaryKey = field;
                break;
            }
        }
        String sqlQuery = "select * from "+aClass.getSimpleName()+ " where "+parimaryKey.getName()+" = " +l;
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next(); //We know only One element present else use  ===>   while (resultSet.next()) {...}
            T t = aClass.getConstructor().newInstance();
        for(Field field : declaredFields){
           field.set(t , resultSet.getObject(field.getName()) );
        }
        return t;
    }
}
