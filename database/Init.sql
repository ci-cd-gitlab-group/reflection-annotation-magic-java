-- Run this Query to any Database where you want to connect your ORM

create table TrnnsactionHistory (
transactionId INT PRIMARY KEY,
accountnumber INT NOT NULL ,
name VARCHAR(30) NOT NULL ,
transactionType VARCHAR(10) NOT NULL ,
amount NUMERIC(10,4) NOT NULL)
;

--    @PrimaryKey
--    long transactionId; //User will not be providing it - will be auto Generate
--   @Column
--    int accountnumber;
--    @Column
--    String name;
--    @Column
--    String transactionType;
--    @Column
--    double amount;